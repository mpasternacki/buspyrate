#!/usr/bin/env python3

import logging
import math
import typing as t
from enum import IntFlag
from pathlib import Path
from struct import pack, unpack
from time import sleep

from buspyrate import BusPirate, Data, SPISpeed

SERIAL_DEV = "/dev/cuaU0"
IMAGE_PATH = Path("~/ic205.bin").expanduser()


logging.basicConfig(level=logging.DEBUG)


class StatusRegister(IntFlag):
    WIP = 1
    WEL = 2
    BP0 = 4
    BP1 = 8
    BP2 = 16
    SRWD = 128


def _pack_cmd_and_addr(cmd: int, addr: int) -> bytes:
    return pack(">BI", cmd, addr << 8)[:-1]


class MX25L8006(BusPirate):
    def rdid(self) -> t.Tuple[int, int]:
        return t.cast(
            t.Tuple[int, int], unpack(">BH", self.spi_write_then_read(b"\x9F", read=3))
        )

    def rdsr(self) -> StatusRegister:
        sr1, sr2 = unpack(">BB", self.spi_write_then_read(b"\x05", 2))
        assert sr1 == sr2, f"0x{sr1:02x} != 0x{sr2:02x}"
        return StatusRegister(sr1)

    def wait_for_wip(self) -> None:
        while self.rdsr() & StatusRegister.WIP != 0:
            sleep(0.5)

    def read(self, addr: int, length: int) -> bytes:
        return self.spi_write_then_read(_pack_cmd_and_addr(0x03, addr), length)

    def wren(self) -> None:
        with self.spi_cs():
            self.spi_transfer(b"\x06")

    def wrdi(self) -> None:
        with self.spi_cs():
            self.spi_transfer(b"\x04")

    def sector_erase(self, addr: int) -> None:
        self.wait_for_wip()
        self.wren()
        with self.spi_cs():
            self.spi_transfer(_pack_cmd_and_addr(0x20, addr))
        self.wait_for_wip()

    def block_erase(self, addr: int) -> None:
        self.wait_for_wip()
        self.wren()
        with self.spi_cs():
            self.spi_transfer(_pack_cmd_and_addr(0x52, addr))
        self.wait_for_wip()

    def chip_erase(self) -> None:
        self.wait_for_wip()
        self.wren()
        with self.spi_cs():
            self.spi_transfer(b"\x60")
        self.wait_for_wip()

    def page_program(self, addr: int, data: Data) -> None:
        assert len(data) <= 256
        self.wait_for_wip()
        self.wren()
        payload = _pack_cmd_and_addr(0x02, addr) + data
        # print(len(payload), payload.hex(), payload)
        self.spi_write_then_read(payload, 0)
        if not self.spi_wtr_0r_confirm:
            sleep(3)  # to make sure everything was sent
        self.wait_for_wip()


if __name__ == "__main__":
    with MX25L8006(SERIAL_DEV) as mx:
        mx.spi(SPISpeed.S250k, output_3v3=True)

        manufacturer_id, device_id = mx.rdid()
        print(f"Manufacturer ID: 0x{manufacturer_id:X}, device ID: 0x{device_id:X}")
        assert manufacturer_id == 0xC2
        assert device_id == 0x2014

        print(mx.rdsr())

        print("Erasing chip ...")
        mx.chip_erase()

        image_size = IMAGE_PATH.stat().st_size
        pages = math.ceil(image_size / 256)
        print(
            f"Programming chip from {IMAGE_PATH}, {image_size} bytes, {pages} pages ..."
        )
        addr = 0
        page = 0
        with IMAGE_PATH.open("rb") as imgf:
            while True:
                data = imgf.read(256)
                if not data:
                    break
                print(f" - page {page}/{pages} at 0x{addr:06X} {int(100*page/pages)}%")
                mx.page_program(addr, data)
                addr += 256
                page += 1

        print(mx.rdsr())
        data = mx.read(0, 512)
        print(len(data), data.hex(), repr(data))
