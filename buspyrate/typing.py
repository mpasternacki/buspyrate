"""BusPyrate types and type aliases"""

__all__ = ["AnyPath", "Data", "SerialDuck"]

import typing as t
from os import PathLike

Data = t.Union[bytes, bytearray]

AnyPath = t.Union[str, bytes, PathLike[str], PathLike[bytes]]


@t.runtime_checkable
class SerialDuck(t.Protocol):
    """Protocol for an object that quacks like pyserial's Serial.

    This way, it should be possible to use other serial connection
    library, as long as the API matches.
    """

    timeout: t.Optional[float]

    # fmt: off
    def close(self) -> None: ...                # noqa
    def read(self, size: int = 1) -> bytes: ... # noqa
    def write(self, data: bytes) -> None: ...   # noqa
    def flush(self) -> None: ...                # noqa
    # fmt: on
