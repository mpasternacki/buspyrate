"""BusPyrate constants and enums"""

__all__ = ["CONFIRM", "SPISpeed"]

from enum import IntEnum

CONFIRM = b"\x01"


class SPISpeed(IntEnum):
    S30k = 0
    S125k = 1
    S250k = 2
    S1M = 3
    S2M = 4
    S2M6 = 5
    S4M = 6
    S8M = 7
