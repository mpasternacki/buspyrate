import typing as t

from buspyrate.typing import AnyPath

# Typing stub for pyserial, only parts actually used by buspyrate - it
# will need updating if we use more features.

PARITY_NONE, PARITY_EVEN, PARITY_ODD, PARITY_MARK, PARITY_SPACE = (
    "N",
    "E",
    "O",
    "M",
    "S",
)
STOPBITS_ONE, STOPBITS_ONE_POINT_FIVE, STOPBITS_TWO = (1, 1.5, 2)
FIVEBITS, SIXBITS, SEVENBITS, EIGHTBITS = (5, 6, 7, 8)

class Serial:
    timeout: t.Optional[float]
    def __init__(
        self,
        port: t.Optional[AnyPath] = None,
        baudrate: int = 9600,
        bytesize: int = EIGHTBITS,
        parity: str = PARITY_NONE,
        stopbits: float = STOPBITS_ONE,
        timeout: t.Optional[float] = None,
        xonxoff: bool = False,
        rtscts: bool = False,
        write_timeout: t.Optional[float] = None,
        dsrdtr: bool = False,
        inter_byte_timeout: t.Optional[float] = None,
    ) -> None: ...
    def open(self) -> None: ...
    def close(self) -> None: ...
    def read(self, size: int = 1) -> bytes: ...
    def write(self, data: bytes) -> None: ...
    def flush(self) -> None: ...
